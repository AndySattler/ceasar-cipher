const cipher = require('./ceasar-cipher')

test('should be able to do a simple shift', () => {
    expect(cipher.ceasarCipher('ABC', 3)).toBe('DEF')
})

test('should be able to do a simple negative shift', () => {
    expect(cipher.ceasarCipher('ABC', -3)).toBe('XYZ')
})

test('should keep non alphabet characters', () => {
    expect(cipher.ceasarCipher('ABC DEF-', 3)).toBe('DEF GHI-')
})

test('should handle wrapping around the alphabet', () => {
    expect(cipher.ceasarCipher('XYZ', 3)).toBe('ABC')
})

test('should handle wrapping around the alphabet backwards', () => {
    expect(cipher.ceasarCipher('ABC', -3)).toBe('XYZ')
})

test('should be case insensitive', () => {
    expect(cipher.ceasarCipher('xyz', 3)).toBe('ABC')
})

test('should throw an exception for to large of a shift', () => {
    expect(()=> {cipher.ceasarCipher('xyz', 27)}).toThrow()
})

test('should throw an exception for to small of a shift', () => {
    expect(()=> {cipher.ceasarCipher('xyz', -27)}).toThrow()
})

