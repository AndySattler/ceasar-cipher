//Code can easily be changed if these assumptions are wrong
//Assumptions:
//this function is case insensitive ABC = abc
//characters outside of the alphabet should be left the way that they are
//strings are short enough that I don't need to worry about concatenating strings being a problem

function ceasarCipher(text, shift)
{
    let cipherResult= ''
    const upperText = text.toUpperCase()

    if(shift < -26 || shift > 26)
        throw 'shift needs to be between -26 and 26'

    for(i=0; i < text.length; i++)
    {
        let charCode = upperText[i].charCodeAt()    

        //only shift letters
        if(charCode >= 'A'.charCodeAt() && charCode <= 'Z'.charCodeAt())
        {
            if(charCode + shift > 'Z'.charCodeAt())
            {
                //wrapped around 
                cipherResult += String.fromCharCode(charCode + shift - 26)
            }
            else if(charCode + shift < 'A'.charCodeAt())
            {
                //wrapped around backwards
                cipherResult += String.fromCharCode(charCode + shift + 26)
            }
            else
                cipherResult += String.fromCharCode(charCode + shift)
        }
        else        
        {
            cipherResult += upperText[i]
        }        
    }

    return cipherResult;
}

exports.ceasarCipher = ceasarCipher